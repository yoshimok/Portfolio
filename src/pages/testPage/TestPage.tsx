import React, { useState } from "react";
import styles from "./TestPage.module.scss";
import { useMediaQuery } from "react-responsive";
import smoothscroll from "smoothscroll-polyfill";

import * as data from "mydata/data";

import { Footer } from "components/organisms/Footer/Footer";
import { Header } from "components/organisms/Header/Header";
import { NavBar } from "../../components/organisms/NavBar/NavBar";

smoothscroll.polyfill();

export type Props = {};

const mockHeaderProps = {
  headerTitle: "テストページです",
  clickHamburger: () => { },
  contents: [
    {
      title: "test",
      href: ``,
    },
    {
      title: "test",
      href: ``,
    },
    {
      title: "test",
      href: ``,
    },
    {
      title: "test",
      href: ``,
    },
    {
      title: "test",
      href: ``,
    },
    {
      title: "test",
      href: ``,
    },
    {
      title: "test",
      href: ``,
    },
    {
      title: "test",
      href: ``,
    },
    {
      title: "test",
      href: ``,
    },
  ],
};

export const TestPage: React.FC<Props> = (props: Props) => {
  const isDesktopOrLaptop = useMediaQuery({
    query: "(min-width: 960px)",
  });


  const [disableNavBar, setDisableNavBar] = useState(true);

  return (
    <div>
      <Header
        {...mockHeaderProps}
        withMenu={isDesktopOrLaptop}
        clickHamburger={() => setDisableNavBar(false)}
      />
      <div className={styles.body}>
        <ul>
          <li>TEst</li>
          <li>TEST</li>
          <li>Te</li>
          <li>TEst</li>
          <li>TEST</li>
          <li>Te</li>
          <li>TEst</li>
          <li>TEST</li>
          <li>Te</li>
          <li>TEst</li>
          <li>TEST</li>
          <li>Te</li>
          <li>TEst</li>
          <li>TEST</li>
          <li>Te</li>
          <li>TEst</li>
          <li>TEST</li>
          <li>Te</li><li>Te</li>
          <li>TEst</li>
          <li>TEST</li>
          <li>Te</li>
          <li>TEst</li>
          <li>TEST</li>
          <li>Te</li><li>Te</li>
          <li>TEst</li>
          <li>TEST</li>
          <li>Te</li>
          <li>TEst</li>
          <li>TEST</li>
          <li>Te</li><li>Te</li>
          <li>TEst</li>
          <li>TEST</li>
          <li>Te</li>
          <li>TEst</li>
          <li>TEST</li>
          <li>Te</li><li>Te</li>
          <li>TEst</li>
          <li>TEST</li>
          <li>Te</li>
          <li>TEst</li>
          <li>TEST</li>
          <li>Te</li>
          <li>TEst</li>
          <li>TEST</li>
          <li>Te</li>
          <li>TEst</li>
          <li>TEST</li>
          <li>Te</li>
          <li>TEst</li>
          <li>TEST</li>
          <li>Te</li><li>TEst</li>
          <li>TEST</li>
          <li>Te</li>
          <li>TEst</li>
          <li>TEST</li>
          <li>Te</li>
          <li>TEst</li>
          <li>TEST</li>
          <li>Te</li>
        </ul>
      </div>
      <Footer copyRight="© 2020 yoshimok" />
    </div>
  );
};
